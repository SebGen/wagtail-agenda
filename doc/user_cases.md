# User cases #

List of user cases.


## User case 1 ##

As a: Website admin

I want to: Have an agenda to show activities of my club


## User case 2 ##

As a: Website visitor

I want to: See next activities of the club and when they are


## User case 3 ##

As a: Website visitor

I want to: See details of an activity, like where it is and how to
access it


## User case 4 ##

As a: Website visitor

I want to: See who organise an activity


## User case 5 ##

As a: Website visitor

I want to: Have details about an activty organizer, like who she/he
is and if he/she have a website


## User case 6 ##

As a: Website visitor

I want to: See all activities an organizer 


## User case 7 ##

As a: Website admin

I want to: Automatically archive past activities and only show present
and future ones on the agenda


## User case 8 ##

As a: Website visitor

I want to: Add an agenda to my own agenda app
