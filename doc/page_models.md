# Page models and models #

Here we have the liste of page models, their goals and their fields.


## AgendaPage ##

A model for an agenda page, with one or more `ActivityPage` as children
pages.

Fields:
* `Title`: The agenda title

Settings:
* `Archives_enabled`: Enable the archive subpage

Methods:
* `activities()`: Get the list of activities linked to this agenda,
  excluding passed activities if archives enabled
* `archived_activities()` Get the list of passed activities linked to
  this agenda if archives enabled, else empty list
  
Context extra properties:
* `is_archives`: Tell if you render the archive of the agenda, or not
* `activities`: List of activities to render

Subpages:
* `archives/`: List all past activities, use the same template as the
  default agenda page


## ActivityLocation ##

A model for location on a map.

Fields:
* `Name`: The of the location
* `Street`: Street, part of the address
* `Street number`: Street number, part of the address
* `Zip`: Zip code, part of the address
* `City`: City, part of the address
* `Country`: Country, part of the address
* `floor`
* `OSM_url`: The URL to location in OpenStreetMap
* `Public transport stops`: Public transport stops nearby
* `Photos`: Photos of the location


## PublicTransportStop ##

A model for public transport stops near a location

Fields:
* `Type`: Train, bus or boat ?
* `Line number`: The number of the line
* `Name`: Name of the line stop 


## ActivityPage ##

A model for an activity page.

Fields:
* `Title`: The activity title
* `Start`: The date and time this activity start
* `Stop`: The date and time this activity stop
* `Organizers`: The organisation, a link to one or many `wagtailperson.models.Person`
* `Description`: The description of the activity
* `Location`: Where this activity is, a link to one `ActivityLocation`
* `Program`: The activity program
