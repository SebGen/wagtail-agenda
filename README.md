# Wagtail Agenda #

An aganda app for Wagtail.

This app is designed to be used in 2 cases:
* The agenda of an organisation, like a club or an association, with
  an archive for past activities
* The programme of an convention, with no archive and all activities
  can be seen on the agenda independently of the actual hour and date

With this app, you can:
* Have one or more aganda
* One or more activities by agenda
* Each agenda can have an archive: If enabled, past activities can
  only be seen on the archive
* Each activity have a location
* Each location can have description, address, floor, the list of
  nearby public transport stops and images
* Each aganda provide an ICS online agenda


## Status ##

The dev of this app just started.

Todo: Everything.


## Install on your Wagtail website ##

TBD


## Install for dev ##

TBD


## Licence ##

LGPL-3.0-only


## Author ##

Sébastien Gendre <seb@k-7.ch>
